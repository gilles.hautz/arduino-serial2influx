#!/bin/bash

#SERIAL="/dev/ttyACM0"
SERIAL="/dev/ttyUSB0"
BAUD=9600
GRAFANA_HOST="blue.lan:8086"

#stty -F $SERIAL raw cs8 115200 ignbrk -brkint -icrnl -imaxbel -opost -onlcr -isig -icanon -iexten -echo -echoe -echok -echoctl -echoke noflsh -ixon -crtscts 
stty -F $SERIAL raw $BAUD -hupcl



while true # loop forever
do
  inputline="" # clear input
 
  # Loop until we get a valid reading from AVR
  until inputline=$(echo $inputline | grep -e "^value01: ")
  do
     inputline=$(head -n 1 < $SERIAL)
  done
	#echo "INPUTLINE = $inputline"
	value01=`echo "$inputline" | cut -f 2 -d " "`
	value01=${value01%$'\r'}
	echo "$value01"
	#rrdtool update demo.rrd N:$cpm
	curl -i -XPOST "http://$GRAFANA_HOST/write?db=monitoring" --data-binary "serial2influx,Board=ArduinoMega,Sensor=LDR value=$value01"
done
